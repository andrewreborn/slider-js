import { Movie } from "../models/movie";

// export function getMovies(movie, cb) {
//   var xhr = new XMLHttpRequest();
//   xhr.open(
//     "GET",
//     "https://fake-movie-database-api.herokuapp.com/api?s=".concat(movie)
//   );

//   xhr.addEventListener("loadend", function () {
//     var data = JSON.parse(xhr.response);

//     var movies = data.Search.map(function (m) {
//       return new Movie(m.imdbID, m.Title, m.Poster);
//     });

//     cb(movies);
//   });

//   xhr.send();
// }

export function getMovies(movie, cb) {
  fetch(`https://fake-movie-database-api.herokuapp.com/api?s=${movie}`)
    .then((res) => res.json())
    .then((data) => {
      // Situazione analoga ad "addEventListener / loadend"

      const movies = data.Search.map(
        (m) => new Movie(m.imdbID, m.Title, m.Poster)
      );

      cb(movies);
    });
}

export function sliceMovies(movies, from, to) {
  var idx = from;
  var goOn = true;
  var ret = [];

  do {
    ret.push(movies[idx]);

    if (idx === to) {
      goOn = false;
    } else if (idx < to || (idx > to && idx < movies.length - 1)) {
      idx++;
    } else {
      idx = 0;
    }
  } while (goOn);

  return ret;
}
