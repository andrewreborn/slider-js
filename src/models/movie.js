export class Movie {
  constructor(id, name, image) {
    this.id = id;
    this.name = name;
    this.image = image;
  }

  getPosterTag() {
    // return '<img src="' + this.image + '">';
    // return '<img src="'.concat(this.image, '">');

    // Backtick (template literal) - SI FA CON ALT + 0096
    // Permette di interpolare dei valori all'interno della stringa
    //  -> Il valore da interpolare deve essere declinato
    //     all'interno di ${ }
    //  -> Nelle tastiere US Intl. il carattere backtick si trova
    //     subito sotto ad ESC
    return `<img class="poster" src="${this.image}">`;
  }
}

// export function Movie(id, name, image) {
//   this.id = id;
//   this.name = name;
//   this.image = image;
// }
