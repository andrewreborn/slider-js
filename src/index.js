// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

function loadMovies(movie, resetIndexes) {
  if (resetIndexes) {
    from = 0;
    to = 3;
  }

  // Svuoto lo slider prima di mettere i film nuovi
  // (quelli richiesti attraverso la select)
  slider.innerHTML = "";

  getMovies(movie, function (apiMovies) {
    movies = apiMovies;

    // sliceMovies(movies, from, to).forEach(function (movie) {
    //   slider.innerHTML +=
    //     '<div class="col-lg-3"><img class="poster" src="' +
    //     movie.image +
    //     '"></div>';
    // });

    const slices = sliceMovies(movies, from, to);

    for (const slice of slices) {
      slider.innerHTML += `
        <div class="col-lg-3">
          ${slice.getPosterTag()}
        </div>
      `;
    }
  });
}

let tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

let slider = document.getElementById("slider");
let from = 0;
let to = 3;
let movies = [];

function handleSliderChange(isNext) {
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML += `
      <div class="col-lg-3">
        ${slice.getPosterTag()}
      </div>
    `;
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next");
  });
});

// Individuo la select nel documento
let sel = document.querySelector('select[name="movies"]');

// Inizializzo lo slider con i film di Batman (almeno qualcosa ce trovo)
loadMovies("batman");

sel.addEventListener("change", function (e) {
  // Carico i film indicati dall'utente con la select
  loadMovies(e.target.value, true);
});
